package ru.tsc.golovina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;

public class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "backup-load";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load backup from XML.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getAdminDataEndpoint().loadBackup(session);
    }

    @Nullable
    public Role[] roles() {
        if (serviceLocator.getSession() == null) {
            return null;
        } else {
            return new Role[]{Role.ADMIN};
        }
    }

}
