package ru.tsc.golovina.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.service.IEndpointLocator;
import ru.tsc.golovina.tm.endpoint.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected IEndpointLocator serviceLocator;

    @Nullable
    public void setIServiceLocator(IEndpointLocator IServiceLocator) {
        this.serviceLocator = IServiceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String getCommand();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    public abstract void execute();

    @NotNull
    @Override
    public String toString() {
        String result = "";
        final String name = getCommand();
        final String arg = getArgument();
        final String description = getDescription();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
