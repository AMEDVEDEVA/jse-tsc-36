package ru.tsc.golovina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getCommand() {
        return "data-json-load";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load json data from file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getAdminDataEndpoint().loadDataJsonJaxB(session);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
