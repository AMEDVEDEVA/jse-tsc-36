package ru.tsc.golovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.endpoint.User;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserDisplayByIdCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-display-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display user by id";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserEndpoint().findUserById(session, id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        showUser(user);
    }

}
