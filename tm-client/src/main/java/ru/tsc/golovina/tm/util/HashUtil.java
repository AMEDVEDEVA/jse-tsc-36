package ru.tsc.golovina.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    static String salt(@NotNull final ISaltSetting setting, @NotNull final String value) {
        @NotNull final String secret = setting.getPasswordSecret();
        @NotNull final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @NotNull
    static String salt(
            @NotNull final String secret,
            @NotNull final Integer iteration,
            @NotNull final String value
    ) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            java.security.MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (byte b : array)
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
