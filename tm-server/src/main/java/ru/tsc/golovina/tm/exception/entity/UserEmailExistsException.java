package ru.tsc.golovina.tm.exception.entity;

import ru.tsc.golovina.tm.exception.AbstractException;

public class UserEmailExistsException extends AbstractException {

    public UserEmailExistsException(final String email) {
        super("Error. USer with email `" + email + "` already exists.");
    }

}
