package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IOwnerRepository;
import ru.tsc.golovina.tm.api.service.IOwnerService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;
import ru.tsc.golovina.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public void add(@Nullable String userId, @NotNull E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.add(userId, entity);
    }

    @Override
    public void addAll(@Nullable String userId, @NotNull List<E> entities) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        for(E entity : entities)
            repository.add(userId, entity);
    }

    @Override
    public boolean existsById(@Nullable String userId, @NotNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return findById(userId, id) != null;
    }

    @Override
    public boolean existsByIndex(@Nullable String userId, @NotNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return findByIndex(userId, index) != null;
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = repository.findById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public void remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(userId, entity);
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }

    @Override
    public E create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = create(userId, name);
        repository.add(entity);
        return entity;
    }

    @NotNull
    @Override
    public E findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @NotNull
    @Override
    public E removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(userId, name);
    }

    @Override
    public @NotNull E removeById(@Nullable String userId, @NotNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.removeById(userId, id);
    }

    @Override
    public @NotNull E removeByIndex(@Nullable String userId, @NotNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final E entity = repository.removeByIndex(userId, index);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name,
                           @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = repository.findById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(description);
    }

    @Override
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name,
                              @NotNull final String description) {
        if (index == null) throw new EmptyIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = repository.findByIndex(index);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(description);
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.existsByName(userId, name);
    }

    @NotNull
    @Override
    public E startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = repository.startById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        return repository.startByIndex(userId, index);
    }

    @NotNull
    @Override
    public E startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.startByName(userId, name);
    }

    @NotNull
    @Override
    public E finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = repository.finishById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        return repository.finishByIndex(userId, index);
    }

    @NotNull
    @Override
    public E finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.finishByName(userId, name);
    }

    @NotNull
    @Override
    public E changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @Nullable final E entity =  repository.changeStatusById(userId, id, status);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index,
                                    @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public E changeStatusByName(@Nullable final String userId, @Nullable final String name,
                                   @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByName(userId, name, status);
    }

}
