package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(@NotNull final IUserService userService, ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    ) {

        sessionService.validate(session, Role.ADMIN);
        userService.add(entity);
    }

    @Override
    @WebMethod
    public void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<User> entities
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.addAll(entities);
    }

    @Override
    @WebMethod
    public @NotNull List<User> findAll(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public @NotNull User findById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findById(id);
    }

    @Override
    @WebMethod
    public @Nullable User findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findByIndex(index);
    }

    @Override
    @WebMethod
    public @NotNull User findUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findUserByLogin(login);
    }

    @Override
    @WebMethod
    public @NotNull User findUserByEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "email") String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findUserByEmail(email);
    }

    @Override
    @WebMethod
    public boolean existsById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.existsById(id);
    }

    @Override
    @WebMethod
    public boolean existsByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.existsByIndex(index);
    }

    @Override
    @WebMethod
    public void clear(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    public void remove(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.remove(entity);
    }

    @Override
    @WebMethod
    public @Nullable User removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeById(id);
    }

    @Override
    @WebMethod
    public @Nullable User removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeByIndex(index);
    }

    @Override
    @WebMethod
    public @NotNull User removeByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeUserByLogin(login);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session, Role.ADMIN);
        return userService.getSize();
    }

    @Override
    @WebMethod
    public @NotNull User create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password);
    }

    @Override
    @WebMethod
    public @NotNull User createWithEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public @NotNull User createWithRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public void setPassword(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.setPassword(id, password);
    }

    @Override
    @WebMethod
    public void setRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "role") Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.setRole(userId, role);
    }

    @Override
    @WebMethod
    public boolean isLoginExists(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.isLoginExists(login);
    }

    @Override
    @WebMethod
    public boolean isEmailExists(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "email") String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.isEmailExists(email);
    }

    @Override
    @WebMethod
    public void updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.updateUserById(session.getUserId(), firstName, lastName, middleName, email);
    }

    @Override
    @WebMethod
    public void updateUserByLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.updateUserByLogin(login, firstName, lastName, middleName, email);
    }

}
