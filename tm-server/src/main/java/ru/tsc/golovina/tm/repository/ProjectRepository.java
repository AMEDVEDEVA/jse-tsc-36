package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.golovina.tm.exception.empty.EmptyNameException;
import ru.tsc.golovina.tm.exception.empty.EmptyUserIdException;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

}
