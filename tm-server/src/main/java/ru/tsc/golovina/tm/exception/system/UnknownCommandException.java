package ru.tsc.golovina.tm.exception.system;

import ru.tsc.golovina.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Incorrect " + command + " command.");
    }

}
