package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    @NotNull
    Session openSession(
            @NotNull @WebParam(name = "login", partName = "login") String login,
            @NotNull @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

}
