package ru.tsc.golovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

}
