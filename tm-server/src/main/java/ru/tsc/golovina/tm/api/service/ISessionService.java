package ru.tsc.golovina.tm.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Session;

public interface ISessionService extends IService<Session> {

    @NotNull
    Session open(@NotNull String login, @NotNull String password);

    @NotNull
    Session sign(@NotNull Session session) throws JsonProcessingException;

    boolean close(@NotNull Session session);

    @NotNull
    boolean checkDataAccess(@NotNull String login, @NotNull String password);

    void validate(@NotNull Session session);

    void validate(@NotNull Session session, @NotNull Role role);

}
