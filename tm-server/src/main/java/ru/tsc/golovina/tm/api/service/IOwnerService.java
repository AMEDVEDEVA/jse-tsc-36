package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator);

    @NotNull
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    void remove(@NotNull String userId, @Nullable E entity);

    @NotNull
    Integer getSize(@NotNull String userId);

    void add(@Nullable String userId, @NotNull E entity);

    void addAll(@Nullable String userId, @NotNull List<E> entities);

    boolean existsById(@Nullable String userId, @NotNull String id);

    boolean existsByIndex(@Nullable String userId, @NotNull Integer index);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    E create(@Nullable String userId, @Nullable String name);

    E create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    E removeById(@Nullable String userId, @NotNull String id);

    @NotNull
    E removeByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    E removeByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    E findByName(@Nullable String userId, @Nullable String name);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    @NotNull
    E startById(@Nullable String userId, @Nullable String id);

    @NotNull
    E startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    E finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    E changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    E changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
